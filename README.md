# FDM Spark Assessment - November 2022

## Data

For this assessment, you will use an airports dataset containing airport codes from around the world. Some of the columns contain attributes identifying airport locations, other codes (IATA, local if exist) that are relevant to identification of an airport. You will also use a cities dataset which contains information about cities around the world with populations greater than 15,000 people.

The `airport-codes.csv` is provided courtesy of [DataHub.io](https://datahub.io/core/airport-codes) under a Public Domain Dedication and License (PDDL). The `cities15000.txt` file is provided courtesy of [GeoNames](https://download.geonames.org/export/dump/) under a [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/). You may find descriptions of attributes and metadata about the airports data in the accompanying [zip archive](https://datahub.io/core/airport-codes/r/airport-codes_zip.zip); likewise, there is a description of all of the cities dataset attributes at the very bottom of the [GeoNames page](https://download.geonames.org/export/dump/).

## Setup

Please refer to the README.md in the [Apache Spark practical repository](https://git.ecdf.ed.ac.uk/akrause/apache-spark-on-cirrus) for getting a Spark and Jupyter Notebook environment setup.

### Alternative Environments

#### Spark Quickstart

If you are having problems with your own Spark and Jupyter Notebook environment setup, as a last resort you could use the 'Live Notebook: DataFrame' environment provided by the [Quickstart with Pyspark](https://spark.apache.org/docs/latest/api/python/getting_started/index.html) documentation. In order to download the data that you need to complete the assessment questions, you can use the following commands in the Binder Notebook there:

```
# data from https://datahub.io/core
!wget https://datahub.io/core/airport-codes/r/airport-codes.csv -O ./airport-codes.csv
# data from https://www.geonames.org
!wget https://download.geonames.org/export/dump/cities15000.zip -O ./cities15000.zip
!unzip ./cities15000.zip
```

#### Google Colaboratory

Yet another alternative environment is [Google Colab](https://colab.research.google.com/). This is a free, public notebook service and you can save your notebooks to your Google Drive if you sign up for a Google account. Simply create a 'New notebook' from the 'File' menu, then add and execute the code below in the first cell:

```
# install required dependencies
!pip install pyspark numpy

# data from https://datahub.io/core
!wget https://datahub.io/core/airport-codes/r/airport-codes.csv -O ./airport-codes.csv
# data from https://www.geonames.org
!wget https://download.geonames.org/export/dump/cities15000.zip -O ./cities15000.zip
!unzip ./cities15000.zip
```

### Spark Session Initialisation

Remember you need to have a Spark session initialised before you can use the Spark contexts which are employed in the lab notebooks. The code you need to initialise these in the notebooks is in step 6 of the [repository README.md](https://git.ecdf.ed.ac.uk/akrause/apache-spark-on-cirrus/-/blob/master/README.md), and here it is for convenience:

```
import os
import sys
 
os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable
 
# Import SparkSession
from pyspark.sql import SparkSession
spark = SparkSession.builder.master("local[4]").appName("FDM_practical").getOrCreate()
sc = spark.sparkContext

# sqlContext object is required for Lab 2 notebooks
sqlContext = SQLContext(spark)
```


## Tips

* Work through the code in the practical lab notebooks in the [Apache Spark practical repository](https://git.ecdf.ed.ac.uk/akrause/apache-spark-on-cirrus)
* Use the PySpark SQL API [documentation](https://spark.apache.org/docs/3.2.0/api/python/reference/pyspark.sql.html)
* Look at the [PySpark Getting Started guide](https://spark.apache.org/docs/latest/api/python/getting_started/index.html)
* For additional reading, [Spark: The Definitve Guide](https://www.oreilly.com/library/view/spark-the-definitive/9781491912201/) by Chambers and Zaharia (2018) is recommended
